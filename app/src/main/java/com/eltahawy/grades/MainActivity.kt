package com.eltahawy.grades

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.internal.BottomNavigationItemView
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView.text="welcome "+intent.getStringExtra("admin")+" !"

        BottomNavigation.setOnNavigationItemSelectedListener(navbottom)
        if(savedInstanceState==null)
            supportFragmentManager.beginTransaction().replace(R.id.container,A_Fragment()).commit()
    }
    private val navbottom =BottomNavigationView.OnNavigationItemSelectedListener {item->
        var selectedFragment:Fragment?= Fragment()
        when(item.itemId){
            R.id.ClASS_A -> {
                selectedFragment=A_Fragment()
            }
            R.id.CLASS_B ->{
                selectedFragment=B_Fragment()
            }
            else -> {
                selectedFragment=C_Fragment()
            }
        }
        supportFragmentManager.beginTransaction().replace(R.id.container,selectedFragment).commit()

        return@OnNavigationItemSelectedListener true

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater=menuInflater
        inflater.inflate(R.menu.insert_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val intent=Intent(this,EditorActivity::class.java).apply{}
        intent.putExtra("state","insert")
        startActivity(intent)
        return super.onOptionsItemSelected(item)
    }
}
