package com.eltahawy.grades


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import kotlinx.android.synthetic.main.custom_list.view.*
import kotlinx.android.synthetic.main.myfragment.*


class B_Fragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.myfragment, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val list = myDB(requireContext()).getData()
        var classB = ArrayList<student>()
        for (student in list) {
            if (student.Class == "B")
                classB.add(student)
        }
        listView.adapter = GradeList(requireActivity(), classB)
        listView.setOnItemLongClickListener { parent, view, position, id ->
            var popupMenu = PopupMenu(requireContext(), view)
            popupMenu.menuInflater.inflate(R.menu.edit_delete_menu, popupMenu.menu)

            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.edit_button -> {
                        val name = view.NameText.text.toString()
                        val intent = Intent(requireContext(), EditorActivity::class.java).apply {}
                        intent.putExtra("state", "edit")
                        intent.putExtra("name", name)
                        intent.putExtra("grade", view.GradeText.text.toString())
                        intent.putExtra("class", "A")
                        startActivity(intent)
                    }

                    R.id.delete_button -> {
                        myDB(requireContext()).deleteData(view.NameText.text.toString())
                    }
                }
                true

            })
            popupMenu.show()
            true

        }
    }
}
