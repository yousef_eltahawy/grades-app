package com.eltahawy.grades

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

val DB_Name="MyDB"
val Table_Name="students"
val Col_Name="name"
val Col_Grade="grade"
val Col_Class="class"
val Col_Id="id"
class myDB(var context:Context):SQLiteOpenHelper(context, DB_Name,null,1) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE  "+ Table_Name +" ("+
                Col_Id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                Col_Name + " VARCHAR(60)," +
                Col_Grade +" VARCHAR(10),"+
                Col_Class+" VARCHAR(2))")

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    fun insert(student:student){
        val db=this.writableDatabase
        val add=ContentValues()
        add.put(Col_Name,student.Name)
        add.put(Col_Grade,student.Grade)
        add.put(Col_Class,student.Class)
        var result=db.insert(Table_Name,null,add)
        if(result == -1.toLong())
            Toast.makeText(context,"Faild",Toast.LENGTH_LONG).show()
        else
            Toast.makeText(context,"sucess",Toast.LENGTH_LONG).show()
        db.close()
    }
    fun getData():ArrayList<student>{
        var list=ArrayList<student>()
        val db=this.readableDatabase
        val result=db.rawQuery("SELECT * FROM $Table_Name",null)
        if(result.moveToFirst()){
            do{
                var student=student()
                student.Name=result.getString(result.getColumnIndex(Col_Name))
                student.Grade=result.getString(result.getColumnIndex(Col_Grade))
                student.Class=result.getString(result.getColumnIndex(Col_Class))
                list.add(student)

            }while (result.moveToNext())}
        result.close()
        db.close()
        return list
    }
    fun deleteData(name:String ){
        val db = this.writableDatabase
        val query="SELECT * FROM $Table_Name WHERE  $Col_Name=\"$name\""
        val result=db.rawQuery(query,null)
        if (result.moveToFirst()) {
            val id = result.getInt(result.getColumnIndex(Col_Id))
            db.delete(
                Table_Name, "$Col_Name = ?",
                arrayOf(name)
            )
            result.close()
            Toast.makeText(context, "deleted", Toast.LENGTH_LONG).show()
        }
        db.close()
    }
    fun upDateData(name:String,student: student){
        val add=ContentValues()
        add.put(Col_Name,student.Name)
        add.put(Col_Grade,student.Grade)
        add.put(Col_Class,student.Class)
        val query="SELECT * FROM $Table_Name WHERE  $Col_Name=\"$name\""
        val db = this.writableDatabase
        val result=db.rawQuery(query,null)
        if (result.moveToFirst()) {
            val id = result.getInt(result.getColumnIndex(Col_Id))
            db.update(Table_Name,add,"$Col_Id =?", arrayOf(id.toString()))
            result.close()
            Toast.makeText(context,"updated",Toast.LENGTH_LONG).show()
        }
        db.close()

    }
}