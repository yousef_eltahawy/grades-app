package com.eltahawy.grades

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import java.util.zip.Inflater
import android.widget.*

class GradeList(val context:Activity,val student: ArrayList<student>)
    :ArrayAdapter<student>(context,R.layout.custom_list,student) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater =context.layoutInflater
        val gradeView=inflater.inflate(R.layout.custom_list,null,true)
        val name =gradeView.findViewById(R.id.NameText) as TextView
        val grade=gradeView.findViewById(R.id.GradeText) as TextView
        name.text=student[position].Name
        grade.text=student[position].Grade
        return gradeView
    }
}