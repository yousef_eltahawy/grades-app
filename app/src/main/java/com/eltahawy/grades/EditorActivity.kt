package com.eltahawy.grades

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_editor.*

class EditorActivity : AppCompatActivity() {

    var Sclass:String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editor)
        spinner()
        if(intent.getStringExtra("state").equals("edit")){
            Edit_name.setText(intent.getStringExtra("name"))
            Edit_grade.setText(intent.getStringExtra("grade"))
            DoneButton.setOnClickListener { view ->
                if(validate(Edit_name))
                    if(validate(Edit_grade)){
                val student = student()
                student.Name = Edit_name.text.toString()
                student.Grade = Edit_grade.text.toString()
                student.Class = Sclass
                val db = myDB(this)
                db.upDateData(intent.getStringExtra("name"),student)
                finish()
                    }

            }
        }
        else{
            DoneButton.setOnClickListener { view ->
                if(validate(Edit_name))
                    if(validate(Edit_grade)){
                val student=student()
                student.Name=Edit_name.text.toString()
                student.Grade=Edit_grade.text.toString()
                student.Class=Sclass
                val db=myDB(this)
                db.insert(student)
                finish()
                    }
            }
        }
    }
    fun spinner(){
        val options=resources.getStringArray(R.array.options)
        val spinner = findViewById(R.id.spinner_class) as Spinner
        spinner_class.adapter=ArrayAdapter(this,android.R.layout.simple_spinner_item,options)
        spinner_class.onItemSelectedListener= object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                Sclass=options[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }
    public fun validate(ET: EditText):Boolean {
        if (ET.text.toString().trim().isEmpty()) {
            ET.error = "Field can't be empty"
            return false;
        } else {
            ET.error = null
            return true
        }
    }
}
