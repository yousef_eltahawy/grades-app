package com.eltahawy.grades

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val button=findViewById<Button>(R.id.button)
        button.setOnClickListener {
            if(validate(editText)){
                val intent= Intent(this,MainActivity::class.java).apply {}
                intent.putExtra("admin",editText.text.toString())
                startActivity(intent)
            }
        }

    }
    public fun validate(ET:EditText):Boolean{
        if(ET.text.toString().trim().isEmpty()){
            ET.error = "Field can't be empty"
            return false;
        }
        else{
            ET.error=null
            return true
        }
    }
}
